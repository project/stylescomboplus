/*
Copyright (c) 2003-2020, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
*/
CKEDITOR.plugins.setLang( 'stylescomboplus', 'fo', {
	label: 'Typografi',
	panelTitle: 'Formatterings stílir',
	panelTitle1: 'Blokk stílir',
	panelTitle2: 'Inline stílir',
	panelTitle3: 'Object stílir'
} );
