(function ($, Drupal, drupalSettings, _) {

  Drupal.behaviors.ckeditorStylesComboPlusSettings = {
    // Used on Settings Page Only.
    attach: function attach(context) {
      var $context = $(context);

      var $ckeditorActiveToolbar = $context.find('.ckeditor-toolbar-configuration').find('.ckeditor-toolbar-active');
      var previousStylesSet = drupalSettings.ckeditor.hiddenCKEditorConfig.stylesSet;
      var that = this;
      $context.find('[name="editor[settings][plugins][stylescomboplus][styles]"]').on('blur.ckeditorStylesComboPlusSettings', function () {
        var styles = $.trim($(this).val());
        var stylesSet = that._generateStylesSetSetting(styles);
        if (!_.isEqual(previousStylesSet, stylesSet)) {
          previousStylesSet = stylesSet;
          $ckeditorActiveToolbar.trigger('CKEditorPluginSettingsChanged', [{ stylesSet: stylesSet }]);
        }
      });
    },
    _generateStylesSetSetting: function _generateStylesSetSetting(styles) {
      var stylesSet = [];

      styles = styles.replace(/\r/g, '\n');
      var lines = styles.split('\n');
      for (var i = 0; i < lines.length; i++) {
        var style = $.trim(lines[i]);

        if (style.length === 0) {
          continue;
        }

        if (style.match(/^ *[a-zA-Z0-9]+ *(\.[a-zA-Z0-9_-]+ *)*\| *.+ *$/) === null) {
          continue;
        }

        var parts = style.split('|');
        var selector = parts[0];
        var label = parts[1];
        var classes = selector.split('.');
        var element = classes.shift();

        stylesSet.push({
          attributes: { class: classes.join(' ') },
          element: element,
          name: label
        });
      }
      return stylesSet;
    }
  };

  Drupal.behaviors.ckeditorStylesComboPlusSettingsSummary = {
    // Used on Settings Page Only.
    attach: function attach() {
      $('[data-ckeditor-plugin-id="stylescomboplus"]').drupalSetSummary(function (context) {
        var styles = $.trim($('[data-drupal-selector="edit-editor-settings-plugins-stylescomboplus-styles"]').val());
        if (styles.length === 0) {
          return Drupal.t('No styles configured');
        }

        var count = $.trim(styles).split('\n').length;
        return Drupal.t('@count styles configured', { '@count': count });
      });
    }
  };


})(jQuery, Drupal, drupalSettings, _);
