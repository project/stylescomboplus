CKEditor Styles Combo Plus

INTRODUCTION
------------
CKEditor Styles Combo Plus is an integration module for the default CKEditor.
found in Drupal 8/9 which replaces the existing Styles drop down widget with
a very similar widget that allows for applying classes to images as well as
other content elements.

BENEFITS
--------
At the moment, the StylesCombo in Drupal CKEditor does not allow for applying
CSS classes to images. If you need this feature (a selector to apply classes to
images), you can install this module and use it without any other changes to
CKEditor or Drupal.


INSTALLATION
------------

Install the CKEditor Styles Combo Plus:
  Download it from https://www.drupal.org/project/stylescomboplus and install
  it on your website.

* With Composer *
  $ composer require 'drupal/stylescomboplus:1.0.x-dev'

CONFIGURATION
-------------
Once you have installed CKEditor Styles Combo Plus, navigate to
Configuration -> Content Authoring -> Text Formats and Editors
(/admin/config/content/formats) to configure the Styles Combo Plus widget and
add the appropriate classes to the styles that are selectable.

KNOWN LIMITATIONS
-----------------
None known at this time. We will update as we find any.
